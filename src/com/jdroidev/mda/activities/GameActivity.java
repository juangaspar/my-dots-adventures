package com.jdroidev.mda.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jdroidev.mda.R;
import com.jdroidev.mda.SharedPreferencesStorage;
import com.jdroidev.mda.game.MusicManager;
import com.jdroidev.mda.game.entities.Dialog;
import com.jdroidev.mda.game.entities.DialogStep;
import com.jdroidev.mda.game.entities.Door;
import com.jdroidev.mda.game.entities.Map;
import com.jdroidev.mda.game.entities.NPC;
import com.jdroidev.mda.game.entities.Player;
import com.jdroidev.mda.game.maps.MapsDefinition;
import com.jdroidev.mda.game.ui.Cell;
import com.jdroidev.mda.game.ui.DialogWriter;
import com.jdroidev.mda.game.ui.DialogWriter.DialogWriterEventListener;

public class GameActivity extends Activity {

  private static final int ACTION_CHECK_DOORS = 1;
  private static final int ACTION_TALK_NPC = 2;
  private static final int ACTION_BATTLE = 3;

  public final int NUMBER_ROWS = 12;
  public final int NUMBER_COLUMNS = 25;

  private final Player player = new Player();
  private ArrayList<Cell> currentPath = null;
  private ArrayList<Cell> usedCells = null;
  private Cell currentCell;
  private Cell finalCell;
  private Map currentLevel = null;

  private Boolean playerIsMoving = false;
  private boolean playerIsTalking = false;
  private NPC selectedNPC;
  private int onFinishMoveAction;
  private int stepsWithoutBattle = 0;
  private boolean inBattle;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game);

    // set first cell
    if (currentLevel == null) {
      currentCell = new Cell(0, 0);
      loadLevel(1);
    }

    setMenuEvents();
    setNPCActionsEvents();
    MusicManager.start(getApplicationContext(), R.raw.background1, true);

  }

  private void setMenuEvents() {
    RelativeLayout playerMenu = (RelativeLayout) findViewById(R.id.playerMenu);
    playerMenu.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
      }
    });

    RelativeLayout closeMenuContainer = (RelativeLayout) findViewById(R.id.closeMenuContainer);
    closeMenuContainer.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        closePlayerMenu();
      }
    });
  }

  private void setNPCActionsEvents() {
    View closeNPCActionsMenuButton = findViewById(R.id.closeNPCActionsTextView);
    closeNPCActionsMenuButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        selectedNPC = null;
        LinearLayout npcActionsMenu = (LinearLayout) findViewById(R.id.npcActionsContainer);
        npcActionsMenu.setVisibility(View.GONE);
      }
    });

    View talkNPCActionsMenuButton = findViewById(R.id.talkNPCActionTextView);
    talkNPCActionsMenuButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        LinearLayout npcActionsMenu = (LinearLayout) findViewById(R.id.npcActionsContainer);
        npcActionsMenu.setVisibility(View.GONE);
        talkToNPC();
      }
    });

  }

  protected void talkToNPC() {
    onFinishMoveAction = ACTION_TALK_NPC;
    moveToNPC();
  }

  private void moveToNPC() {
    // check row and column + 1
    Integer rightRow = selectedNPC.row;
    Integer rightColumn = selectedNPC.column + 1;
    Cell rightCell = new Cell(rightRow, rightColumn);

    if (isSameCell(currentCell, rightCell)) {
      startNPCDialog();
      return;
    }

    // check row + 1 and column
    Integer downRow = selectedNPC.row + 1;
    Integer downColumn = selectedNPC.column;
    Cell downCell = new Cell(downRow, downColumn);

    if (isSameCell(currentCell, downCell)) {
      startNPCDialog();
      return;
    }

    // check row and column - 1
    Integer leftRow = selectedNPC.row;
    Integer leftColumn = selectedNPC.column - 1;
    Cell leftCell = new Cell(leftRow, leftColumn);

    if (isSameCell(currentCell, leftCell)) {
      startNPCDialog();
      return;
    }

    // check row - 1 and column
    Integer upRow = selectedNPC.row - 1;
    Integer upColumn = selectedNPC.column;
    Cell upCell = new Cell(upRow, upColumn);

    if (isSameCell(currentCell, upCell)) {
      startNPCDialog();
      return;
    }

    if (isCellAvailable(rightCell) == true) {

      moveTo(String.valueOf(rightRow), String.valueOf(rightColumn));
      return;
    }

    if (isCellAvailable(downCell) == true) {
      moveTo(String.valueOf(downRow), String.valueOf(downColumn));
      return;
    }

    if (isCellAvailable(leftCell) == true) {
      moveTo(String.valueOf(leftRow), String.valueOf(leftColumn));
      return;
    }

    if (isCellAvailable(upCell) == true) {
      moveTo(String.valueOf(upRow), String.valueOf(upColumn));
      return;
    }
  }

  private boolean isSameCell(Cell cell1, Cell cell2) {

    if (cell1.row == cell2.row && cell1.column == cell2.column)
      return true;

    return false;
  }

  private void setUpLayout() {

    for (int i = 0; i < this.NUMBER_ROWS; i++) {
      for (int j = 0; j < this.NUMBER_COLUMNS; j++) {
        String cellId = "r" + i + "_c" + j;

        LinearLayout cell = (LinearLayout) findViewById(getResources()
            .getIdentifier(cellId, "id", "com.jdroidev.mda"));

        // set cell background
        cell.setBackgroundDrawable(null);

        // check if is blocked
        int[] currentLevelBlockedCells = currentLevel.blockedCells;
        int cellNumber = (i * NUMBER_COLUMNS) + j;
        int isCellBlocked = currentLevelBlockedCells[cellNumber];

        if (isCellBlocked >= 0 && i == currentCell.row
            && j == currentCell.column) {
          cell.setBackgroundResource(R.drawable.char_down);
        }

        cell.setTag(cellId);

        cell.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View cell) {

            if (playerIsMoving)
              return;

            if (playerIsTalking) {
              // Add a character every 150ms
              DialogWriter writer = (DialogWriter) findViewById(R.id.dialogContainer);
              writer.setCharacterDelay(1);
              writer.setOnFinishWaitingTime(100);
              return;
            }

            // hide actions menu
            LinearLayout npcActionsMenu = (LinearLayout) findViewById(R.id.npcActionsContainer);
            npcActionsMenu.setVisibility(View.GONE);

            String cellId = (String) cell.getTag();

            String[] cellIdParts = cellId.split("_");
            String cellRow = cellIdParts[0].substring(1,
                cellIdParts[0].length());
            String cellColumn = cellIdParts[1].substring(1,
                cellIdParts[1].length());

            Cell clickedCell = new Cell(Integer.parseInt(cellRow), Integer
                .parseInt(cellColumn));

            if (checkNPCs(clickedCell)) {
              return;
            } else {
              selectedNPC = null;
            }

            if (Integer.parseInt(cellRow) == currentCell.row
                && Integer.parseInt(cellColumn) == currentCell.column) {
              showPlayerMenu();
            } else if (!isCellAvailable(clickedCell)) {
            } else {
              onFinishMoveAction = ACTION_CHECK_DOORS;
              moveTo(cellRow, cellColumn);
            }
          }
        });

        cell.setOnTouchListener(new OnTouchListener() {

          @Override
          public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
              // show topo indicating order type
              RelativeLayout topoOrder = (RelativeLayout) findViewById(R.id.topoOrder);
              RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                  20, 20);
              params.setMargins((int) event.getRawX(), (int) event.getRawY(),
                  0, 0);
              topoOrder.setLayoutParams(params);
              topoOrder.setVisibility(View.VISIBLE);
              AlphaAnimation alpha = new AlphaAnimation(0.8F, 0);
              alpha.setDuration(400); // Make animation instant
              alpha.setFillAfter(true); // Tell it to persist after the
              // animation
              topoOrder.startAnimation(alpha);
            }
            return false;
          }
        });
      }
    }

    // set NPC images
    NPC[] currentLevelNPCs = currentLevel.NPCs;
    for (int i = 0; i < currentLevelNPCs.length; i++) {
      NPC npc = currentLevelNPCs[i];

      String cellId = "r" + npc.row + "_c" + npc.column;

      LinearLayout cell = (LinearLayout) findViewById(getResources()
          .getIdentifier(cellId, "id", "com.jdroidev.mda"));

      cell.setBackgroundResource(npc.imageResource);
    }

  }

  protected boolean checkNPCs(Cell clickedCell) {

    NPC[] currentLevelNPCs = currentLevel.NPCs;
    for (int i = 0; i < currentLevelNPCs.length; i++) {
      NPC npc = currentLevelNPCs[i];

      if (npc.row == clickedCell.row && npc.column == clickedCell.column) {

        // check distance between npc and player
        Integer distance = Math.abs(npc.row - currentCell.row)
            + Math.abs(npc.column - currentCell.column);

        if (distance > 4)
          return true;

        showNPCActions(npc);
        return true;
      }
    }
    return false;
  }

  private void showNPCActions(NPC npc) {
    selectedNPC = npc;

    LinearLayout npcActionsMenu = (LinearLayout) findViewById(R.id.npcActionsContainer);
    npcActionsMenu.setVisibility(View.VISIBLE);

  }

  protected void showDialog(Dialog dialog) {
    // Add a character every 150ms
    DialogWriter writer = (DialogWriter) findViewById(R.id.dialogContainer);
    writer.setVisibility(View.VISIBLE);

    writer.addEventListener(new DialogWriterEventListener() {

      @Override
      public void onWritingFinishEvent() {
        playerIsTalking = false;
      }
    });

    writer.showDialog(dialog);
  }

  protected void loadLevel(int i) {

    // set light filter

    Calendar c = Calendar.getInstance();
    int hour = c.get(Calendar.HOUR_OF_DAY);

    if (hour >= 6 && hour < 10) {
      LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);
      lightFilter.setBackgroundColor(Color.parseColor("#FFCC66"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.3F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 10 && hour < 18) {
      LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);
      lightFilter.setBackgroundColor(Color.TRANSPARENT);
    } else if (hour >= 18 && hour < 21) {
      LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);
      lightFilter.setBackgroundColor(Color.parseColor("#FFCC99"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.4F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 21 && hour < 24) {
      LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);
      lightFilter.setBackgroundColor(Color.parseColor("#000066"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.3F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 0 && hour < 6) {
      LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);
      lightFilter.setBackgroundColor(Color.parseColor("#000033"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.5F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    }

    currentLevel = MapsDefinition.getMap(i);

    // set background
    RelativeLayout parentContainer = (RelativeLayout) findViewById(R.id.parentContainer);

    try {
      InputStream is = getAssets().open("maps/" + currentLevel.background);

      Drawable d = Drawable.createFromStream(is, null);
      parentContainer.setBackgroundDrawable(d);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    setUpLayout();
  }

  protected void moveTo(String cellRow, String cellColumn) {
    currentPath = new ArrayList<Cell>();
    usedCells = new ArrayList<Cell>();

    finalCell = new Cell(Integer.parseInt(cellRow),
        Integer.parseInt(cellColumn));
    checkNextStep(currentCell);
  }

  private void checkNextStep(Cell nextCell) {
    // add to current path if not
    if (currentPath.isEmpty() || !isCellInArray(nextCell, currentPath))
      currentPath.add(nextCell);

    // set up best cell parameters
    Cell bestCell = null;
    int bestCellPoints = 0;

    // check row and column + 1
    Integer rightRow = nextCell.row;
    Integer rightColumn = nextCell.column + 1;
    Cell rightCell = new Cell(rightRow, rightColumn);
    if (isCellInArray(rightCell, currentPath) == false
        && isCellInArray(rightCell, usedCells) == false
        && isCellAvailable(rightCell) == true) {

      Integer points = Math.abs(finalCell.row - rightRow)
          + Math.abs(finalCell.column - rightColumn);

      if (points > 0) {
        points = getPointsFromNextCell(rightCell);
      }

      if (points != null && (bestCell == null || points < bestCellPoints)) {
        bestCell = rightCell;
        bestCellPoints = points;
      }
    }

    // check row + 1 and column
    Integer downRow = nextCell.row + 1;
    Integer downColumn = nextCell.column;
    Cell downCell = new Cell(downRow, downColumn);
    if (isCellInArray(downCell, currentPath) == false
        && isCellInArray(downCell, usedCells) == false
        && isCellAvailable(downCell) == true) {

      Integer points = Math.abs(finalCell.row - downRow)
          + Math.abs(finalCell.column - downColumn);

      if (points > 0) {
        points = getPointsFromNextCell(downCell);
      }

      if (points != null && (bestCell == null || points < bestCellPoints)) {
        bestCell = downCell;
        bestCellPoints = points;
      }
    }

    // check row and column - 1
    Integer leftRow = nextCell.row;
    Integer leftColumn = nextCell.column - 1;
    Cell leftCell = new Cell(leftRow, leftColumn);
    if (isCellInArray(leftCell, currentPath) == false
        && isCellInArray(leftCell, usedCells) == false
        && isCellAvailable(leftCell) == true) {

      Integer points = Math.abs(finalCell.row - leftRow)
          + Math.abs(finalCell.column - leftColumn);

      if (points > 0) {
        points = getPointsFromNextCell(leftCell);
      }

      if (points != null && (bestCell == null || points < bestCellPoints)) {
        bestCell = leftCell;
        bestCellPoints = points;
      }
    }

    // check row - 1 and column
    Integer upRow = nextCell.row - 1;
    Integer upColumn = nextCell.column;
    Cell upCell = new Cell(upRow, upColumn);
    if (isCellInArray(upCell, currentPath) == false
        && isCellInArray(upCell, usedCells) == false
        && isCellAvailable(upCell) == true) {

      Integer points = Math.abs(finalCell.row - upRow)
          + Math.abs(finalCell.column - upColumn);

      if (points > 0) {
        points = getPointsFromNextCell(upCell);
      }

      if (points != null && (bestCell == null || points < bestCellPoints)) {
        bestCell = upCell;
        bestCellPoints = points;
      }
    }

    if (bestCell != null) {
      if (bestCellPoints == 0) {
        currentPath.add(bestCell);
        move();
      } else {
        if (checkBattle()) {
          onFinishMoveAction = GameActivity.ACTION_BATTLE;
          currentPath.add(bestCell);
          move();
        } else {
          checkNextStep(bestCell);
        }
      }

    } else {
      currentPath.remove(currentPath.size() - 1);
      usedCells.add(nextCell);

      checkNextStep(currentPath.get(currentPath.size() - 1));
    }

  }

  private Integer getPointsFromNextCell(Cell cell) {
    Integer bestCellPoints = null;

    // check row and column + 1
    Integer rightRow = cell.row;
    Integer rightColumn = cell.column + 1;
    Cell rightCell = new Cell(rightRow, rightColumn);
    if (isCellInArray(rightCell, currentPath) == false
        && isCellInArray(rightCell, usedCells) == false
        && isCellAvailable(rightCell) == true) {

      Integer points = Math.abs(finalCell.row - rightRow)
          + Math.abs(finalCell.column - rightColumn);

      if (bestCellPoints == null || points < bestCellPoints) {
        bestCellPoints = points + 1;
      }
    }

    // check row + 1 and column
    Integer downRow = cell.row + 1;
    Integer downColumn = cell.column;
    Cell downCell = new Cell(downRow, downColumn);
    if (isCellInArray(downCell, currentPath) == false
        && isCellInArray(downCell, usedCells) == false
        && isCellAvailable(downCell) == true) {

      Integer points = Math.abs(finalCell.row - downRow)
          + Math.abs(finalCell.column - downColumn);

      if (bestCellPoints == null || points < bestCellPoints) {
        bestCellPoints = points + 1;
      }
    }

    // check row and column - 1
    Integer leftRow = cell.row;
    Integer leftColumn = cell.column - 1;
    Cell leftCell = new Cell(leftRow, leftColumn);
    if (isCellInArray(leftCell, currentPath) == false
        && isCellInArray(leftCell, usedCells) == false
        && isCellAvailable(leftCell) == true) {

      Integer points = Math.abs(finalCell.row - leftRow)
          + Math.abs(finalCell.column - leftColumn);

      if (bestCellPoints == null || points < bestCellPoints) {
        bestCellPoints = points + 1;
      }
    }

    // check row - 1 and column
    Integer upRow = cell.row - 1;
    Integer upColumn = cell.column;
    Cell upCell = new Cell(upRow, upColumn);
    if (isCellInArray(upCell, currentPath) == false
        && isCellInArray(upCell, usedCells) == false
        && isCellAvailable(upCell) == true) {

      Integer points = Math.abs(finalCell.row - upRow)
          + Math.abs(finalCell.column - upColumn);

      if (bestCellPoints == null || points < bestCellPoints) {
        bestCellPoints = points + 1;
      }
    }

    return bestCellPoints;
  }

  private boolean isCellInArray(Cell cell, ArrayList<Cell> cellList) {
    for (Cell listCell : cellList) {
      if (cell.row == listCell.row && cell.column == listCell.column)
        return true;
    }
    return false;
  }

  private boolean isCellAvailable(Cell cell) {

    // check if cell is out of level
    if (cell.row < 0 || cell.row >= NUMBER_ROWS || cell.column < 0
        || cell.column >= NUMBER_COLUMNS)
      return false;

    // check if is blocked
    int[] currentLevelBlockedCells = currentLevel.blockedCells;
    int cellNumber = (cell.row * NUMBER_COLUMNS) + cell.column;
    int isCellBlocked = currentLevelBlockedCells[cellNumber];

    if (isCellBlocked > 0)
      return false;

    // check npc cells
    NPC[] currentLevelNPCs = currentLevel.NPCs;
    for (int i = 0; i < currentLevelNPCs.length; i++) {
      NPC npc = currentLevelNPCs[i];
      if (npc.row == cell.row && npc.column == cell.column) {
        return false;
      }
    }

    return true;
  }

  private void move() {
    playerIsMoving = true;
    for (int i = 0; i < currentPath.size(); i++) {
      drawMove((i * 120), i);
    }

    currentCell = currentPath.get(currentPath.size() - 1);
  }

  private boolean checkDoors(int i) {

    Cell currentCell = currentPath.get(i);

    Door[] currentLevelDoors = currentLevel.doors;
    for (int j = 0; j < currentLevelDoors.length; j++) {
      Door door = currentLevelDoors[j];

      if (door.row == currentCell.row && door.column == currentCell.column) {
        goToDoor(door);
        return true;
      }
    }
    return false;
  }

  private void goToDoor(final Door door) {
    currentPath = new ArrayList<Cell>();
    usedCells = new ArrayList<Cell>();

    // show level transition
    RelativeLayout levelTransition = (RelativeLayout) findViewById(R.id.levelTransition);
    levelTransition.setVisibility(View.VISIBLE);
    AlphaAnimation alphaTransition = new AlphaAnimation(0, 1);
    alphaTransition.setAnimationListener(new AnimationListener() {

      @Override
      public void onAnimationStart(Animation arg0) {
        // TODO Auto-generated method stub

      }

      @Override
      public void onAnimationRepeat(Animation arg0) {
        // TODO Auto-generated method stub

      }

      @Override
      public void onAnimationEnd(Animation arg0) {
        // show topo indicating order type
        RelativeLayout levelTransition = (RelativeLayout) findViewById(R.id.levelTransition);
        levelTransition.setVisibility(View.VISIBLE);
        AlphaAnimation alphaTransition = new AlphaAnimation(1, 0);
        alphaTransition.setDuration(750); // Make animation instant
        alphaTransition.setFillAfter(true); // Tell it to persist after
        // the
        // animation
        levelTransition.startAnimation(alphaTransition);
        currentCell = new Cell(door.nextLevelPlayerRow,
            door.nextLevelPlayerColumn);
        playerIsMoving = false;
        loadLevel(door.nextLevel);
      }
    });
    alphaTransition.setDuration(750); // Make animation instant
    alphaTransition.setFillAfter(true); // Tell it to persist after the
    // animation
    levelTransition.startAnimation(alphaTransition);

  }

  /**
   * Espera y cierra la aplicación tras los milisegundos indicados
   * 
   * @param milisegundos
   * @return
   */
  public boolean drawMove(int miliseconds, final int cellIndex) {

    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {

        if (currentPath.size() == 0 || inBattle)
          return;

        Cell cell = currentPath.get(cellIndex);

        if (cellIndex > 0) {
          Cell lastCell = currentPath.get(cellIndex - 1);
          String lastCellId = "r" + lastCell.row + "_c" + lastCell.column;

          LinearLayout lastCellLayout = (LinearLayout) findViewById(getResources()
              .getIdentifier(lastCellId, "id", "com.jdroidev.mda"));

          // set cell background
          lastCellLayout.setBackgroundDrawable(null);
        }

        String cellId = "r" + cell.row + "_c" + cell.column;

        LinearLayout cellLayout = (LinearLayout) findViewById(getResources()
            .getIdentifier(cellId, "id", "com.jdroidev.mda"));

        // switch character image depending on direction
        if (cellIndex > 0) {

          // check if is blocked
          int[] currentLevelBlockedCells = currentLevel.blockedCells;
          int cellNumber = (cell.row * NUMBER_COLUMNS) + cell.column;
          int isCellBlocked = currentLevelBlockedCells[cellNumber];

          if (isCellBlocked != -1) {
            Cell lastCell = currentPath.get(cellIndex - 1);

            if (lastCell.row == cell.row) {
              if (lastCell.column < cell.column)
                cellLayout.setBackgroundResource(R.drawable.char_right);
              else
                cellLayout.setBackgroundResource(R.drawable.char_left);
            }

            if (lastCell.column == cell.column) {
              if (lastCell.row < cell.row)
                cellLayout.setBackgroundResource(R.drawable.char_down);
              else
                cellLayout.setBackgroundResource(R.drawable.char_up);
            }
          }
        }

        if (cellIndex == currentPath.size() - 1) {

          switch (onFinishMoveAction) {
          case ACTION_CHECK_DOORS:
            checkDoors(cellIndex);
            break;

          case ACTION_TALK_NPC:
            startNPCDialog();
            break;

          case ACTION_BATTLE:
            launchBattle();
            break;
          default:
            break;
          }
          playerIsMoving = false;
        }
      }
    }, miliseconds);
    return true;
  }

  protected void startNPCDialog() {

    playerIsTalking = true;

    // ADDED FOR TESTING PURPOSE
    Dialog dialog = new Dialog();
    dialog.steps.add(new DialogStep(R.drawable.char_down,
        "Me encanta que las cosas funcionen"));
    dialog.steps.add(new DialogStep(selectedNPC.imageResource,
        "Esto va a ser de traca!!"));
    dialog.steps.add(new DialogStep(R.drawable.char_down, "Ya te digo!!"));
    dialog.steps
    .add(new DialogStep(selectedNPC.imageResource, "Que dices tu??"));
    dialog.steps.add(new DialogStep(R.drawable.char_down,
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHH!!!!!!"));

    showDialog(dialog);

  }

  protected void showPlayerMenu() {
    RelativeLayout playerMenu = (RelativeLayout) findViewById(R.id.playerMenu);
    playerMenu.setVisibility(View.VISIBLE);
  }

  protected void closePlayerMenu() {
    RelativeLayout playerMenu = (RelativeLayout) findViewById(R.id.playerMenu);
    playerMenu.setVisibility(View.GONE);
  }

  @Override
  protected void onPause() {
    MusicManager.pause();
    super.onPause();
  }

  public boolean checkBattle() {
    if (currentLevel.battleProbability == 0)
      return false;

    Double battleProbability = currentLevel.battleProbability
        * stepsWithoutBattle * Math.random();

    if (battleProbability > 30) {
      return true;
    } else {
      stepsWithoutBattle++;
      return false;
    }

  }

  private void launchBattle() {

    Intent intent = new Intent(getApplicationContext(), BattleActivity.class);
    intent.putExtra("player", SharedPreferencesStorage.objectToJson(player));
    intent.putExtra("mapId", currentLevel.id);
    startActivity(intent);
    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    Toast.makeText(getApplicationContext(), "A LA BATALLA!!!!",
        Toast.LENGTH_LONG).show();

    stepsWithoutBattle = 0;
  }
}
