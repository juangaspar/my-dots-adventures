package com.jdroidev.mda.activities;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.jdroidev.mda.R;
import com.jdroidev.mda.SharedPreferencesStorage;
import com.jdroidev.mda.game.EnemyFactory;
import com.jdroidev.mda.game.entities.Enemy;
import com.jdroidev.mda.game.entities.Map;
import com.jdroidev.mda.game.entities.Player;
import com.jdroidev.mda.game.maps.MapsDefinition;

public class BattleActivity extends Activity {

  private Player player;
  private Map map;
  private ArrayList<Enemy> enemies;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_battle);

    loadPlayer();
    loadMap();
    loadEnemies();
  }

  private void loadEnemies() {
    enemies = EnemyFactory.getEnemiesForLevel(map);

  }

  private void loadMap() {
    Integer mapId = getIntent().getExtras().getInt("mapId");
    map = MapsDefinition.getMap(mapId);

    RelativeLayout parentContainer = (RelativeLayout) findViewById(R.id.parentContainer);
    parentContainer.setBackgroundResource(map.battleBackgroundResources[0]);

    // set light filter

    Calendar c = Calendar.getInstance();
    int hour = c.get(Calendar.HOUR_OF_DAY);
    LinearLayout lightFilter = (LinearLayout) findViewById(R.id.lightFilter);

    if (hour >= 6 && hour < 10) {
      lightFilter.setBackgroundColor(Color.parseColor("#FFCC66"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.3F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 10 && hour < 18) {
      lightFilter.setBackgroundColor(Color.TRANSPARENT);
    } else if (hour >= 18 && hour < 21) {
      lightFilter.setBackgroundColor(Color.parseColor("#FFCC99"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.4F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 21 && hour < 24) {
      lightFilter.setBackgroundColor(Color.parseColor("#000066"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.3F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    } else if (hour >= 0 && hour < 6) {
      lightFilter.setBackgroundColor(Color.parseColor("#000033"));
      AlphaAnimation alpha = new AlphaAnimation(0, 0.5F);
      alpha.setDuration(0);
      alpha.setFillAfter(true);
      lightFilter.startAnimation(alpha);
    }

  }

  private void loadPlayer() {
    String playerJSON = getIntent().getStringExtra("player");
    player = SharedPreferencesStorage.JsonToObject(playerJSON, Player.class);

  }
}
