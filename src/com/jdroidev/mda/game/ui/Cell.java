package com.jdroidev.mda.game.ui;

public class Cell {

  public Cell(Integer row, Integer column) {
    this.row = row;
    this.column = column;
  }

  public Integer row;
  public Integer column;
}
