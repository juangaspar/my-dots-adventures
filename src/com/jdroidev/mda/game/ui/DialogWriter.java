package com.jdroidev.mda.game.ui;

import java.util.ArrayList;
import java.util.EventListener;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jdroidev.mda.R;
import com.jdroidev.mda.game.entities.Dialog;
import com.jdroidev.mda.game.entities.DialogStep;

public class DialogWriter extends LinearLayout {

  private static final long DEFAULT_DELAY = 100;
  private static final long DEFAULT_WAITING_TIME = 1000;
  private CharSequence mText;
  private int mIndex;
  private long mDelay = DEFAULT_DELAY; // Default 500ms delay
  private long mWaitingTime = DEFAULT_WAITING_TIME;
  private TextView textView;
  private ImageView imageView;

  public DialogWriter(Context context) {
    super(context);
  }

  public DialogWriter(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  private final Handler mHandler = new Handler();
  private final Runnable characterAdder = new Runnable() {

    @Override
    public void run() {
      textView.setText(mText.subSequence(0, mIndex++));

      if (mIndex == mText.length()) {
        mHandler.postDelayed(new Runnable() {
          @Override
          public void run() {
            if (currentDialogStep == currentDialogSteps.size()) {
              View dialogContainer = findViewById(R.id.dialogContainer);
              dialogContainer.setVisibility(View.GONE);
              dispatchEventWritingFinishEvent();
            } else {
              showDialogStep(currentDialogStep);
            }
          }
        }, mWaitingTime);
      }

      if (mIndex <= mText.length()) {
        mHandler.postDelayed(characterAdder, mDelay);
      }
    }
  };

  private ArrayList<DialogStep> currentDialogSteps;
  private int currentDialogStep;

  public void animateText(CharSequence text) {
    mText = text;
    mIndex = 0;
    mDelay = DEFAULT_DELAY;
    mWaitingTime = DEFAULT_WAITING_TIME;

    MediaPlayer mp = MediaPlayer.create(getContext(), R.raw.texting);
    mp.start();

    textView = (TextView) findViewById(R.id.dialogTextView);
    textView.setText("");
    mHandler.removeCallbacks(characterAdder);
    mHandler.postDelayed(characterAdder, mDelay);
  }

  public void setCharacterDelay(long millis) {
    mDelay = millis;
  }

  public void setOnFinishWaitingTime(long millis) {
    mWaitingTime = millis;
  }

  public void showDialog(Dialog dialog) {
    currentDialogSteps = dialog.steps;
    currentDialogStep = 0;
    showDialogStep(currentDialogStep);
  }

  private void showDialogStep(int currentDialogStep) {
    int dialogImage = currentDialogSteps.get(currentDialogStep).characterImage;
    imageView = (ImageView) findViewById(R.id.dialogImageView);
    imageView.setImageResource(dialogImage);

    String text = currentDialogSteps.get(currentDialogStep).text;
    animateText(text);
    this.currentDialogStep++;
  }

  public interface DialogWriterEventListener extends EventListener {

    public void onWritingFinishEvent();
  }

  protected ArrayList<DialogWriterEventListener> eventListenerList = new ArrayList<DialogWriterEventListener>();

  /**
   * Adds the eventListenerList for MapViewController
   * 
   * @param listener
   */
  public void addEventListener(DialogWriterEventListener listener) {
    eventListenerList.add(listener);
  }

  /**
   * Removes the eventListenerList for MapViewController
   * 
   * @param listener
   */
  public void removeEventListener(DialogWriterEventListener listener) {
    eventListenerList.remove(listener);
  }

  /**
   * Dispatches CONNECTION and LOCATION events
   * 
   * @param event
   * @param message
   */
  public void dispatchEventWritingFinishEvent() {

    for (DialogWriterEventListener listener : eventListenerList) {
      listener.onWritingFinishEvent();
    }
  }

}