package com.jdroidev.mda.game;

import java.util.ArrayList;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class MusicManager {
  private static final String TAG = "MusicManager";

  private static ArrayList<MediaPlayer> players = new ArrayList<MediaPlayer>();

  public static void start(Context context, int music) {
    start(context, music, false);
  }

  public static void start(Context context, int music, boolean loop) {

    MediaPlayer mp = new MediaPlayer();
    mp = MediaPlayer.create(context, music);
    players.add(mp);

    try {
      mp.setLooping(loop);
      mp.start();
    } catch (Exception e) {
      Log.e(TAG, e.getMessage(), e);
    }

  }

  public static void pause() {
    ArrayList<MediaPlayer> mps = players;
    for (MediaPlayer p : mps) {
      if (p.isPlaying()) {
        p.pause();
      }
    }
  }

  public static void release() {
    ArrayList<MediaPlayer> mps = players;
    for (MediaPlayer mp : mps) {
      try {
        if (mp != null) {
          if (mp.isPlaying()) {
            mp.stop();
          }
          mp.release();
        }
      } catch (Exception e) {
        Log.e(TAG, e.getMessage(), e);
      }
    }
    mps.clear();
  }
}