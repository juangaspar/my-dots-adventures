package com.jdroidev.mda.game.entities;

public class DialogStep {

  public DialogStep(int characterImage, String text) {
    this.characterImage = characterImage;
    this.text = text;
  }

  public Integer characterImage = null;
  public String text = null;
}
