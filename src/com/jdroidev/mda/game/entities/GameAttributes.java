package com.jdroidev.mda.game.entities;

public class GameAttributes {
  public Integer energy = 100;
  public Integer maxEnergy = 100;
  public Integer attackLevel = 1;
  public Integer defenseLevel = 1;
}
