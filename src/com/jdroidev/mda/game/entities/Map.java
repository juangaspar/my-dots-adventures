package com.jdroidev.mda.game.entities;

public abstract class Map {
  public int id;
  public Door[] doors;
  public int[] blockedCells;
  public NPC[] NPCs;
  public String background;
  public Integer battleLevel;
  public Integer battleProbability;
  public int[] battleBackgroundResources;
}