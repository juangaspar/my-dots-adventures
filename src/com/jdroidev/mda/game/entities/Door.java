package com.jdroidev.mda.game.entities;

public class Door {

  public Door(int row, int column, int nextLevel, int nextLevelPlayerRow,
      int nextLevelPlayerColumn) {
    this.row = row;
    this.column = column;
    this.nextLevel = nextLevel;
    this.nextLevelPlayerRow = nextLevelPlayerRow;
    this.nextLevelPlayerColumn = nextLevelPlayerColumn;
  }

  public Integer row;
  public Integer column;
  public Integer nextLevel;
  public Integer nextLevelPlayerRow;
  public Integer nextLevelPlayerColumn;
}
