package com.jdroidev.mda.game.entities;

public class NPC {

  public NPC(int row, int column, int imageResource) {
    this.row = row;
    this.column = column;
    this.imageResource = imageResource;
  }

  public Integer row;
  public Integer column;
  public Integer imageResource;
}
