package com.jdroidev.mda.game.maps;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.jdroidev.mda.game.entities.Map;

public class MapsDefinition {
  private static Class<?>[] maps = new Class<?>[] { Map_1.class, Map_2.class,
    Map_3.class };

  public static Map getMap(Integer mapId) {
    Constructor constructor = null;
    try {
      constructor = maps[mapId - 1].getConstructor();
    } catch (SecurityException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (NoSuchMethodException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    Map mapObject = null;
    try {
      mapObject = (Map) constructor.newInstance();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return mapObject;
  }
}